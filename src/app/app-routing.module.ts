import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PlacesComponent } from './components/places/places.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PlaceDetailComponent } from './components/place-detail/place-detail.component';
import { PlacesResolverService } from './resolvers/places-resolver.service';

const routes: Routes = [
  { 
    path: 'places', 
    component: PlacesComponent,
    resolve: {
      places: PlacesResolverService
    }
   },
  { path: 'places/:id', component: PlaceDetailComponent },
  { 
    path: 'welcome', 
    component: WelcomeComponent,
    data: { title: 'Places' } 
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
  { path: '',   redirectTo: '/welcome', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
