import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wikilink'
})
export class WikilinkPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return '<a href=' + 
      '"https://en.wikipedia.org/w/index.php?search=' + value + '&go=Go"' + 
      ' target="_blank">' + value + "</a>";
  }

}
