export class Place {
    id: number;
    name: string;
    visits: number;
}