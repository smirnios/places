import { Component, OnInit } from '@angular/core';
import { PlacesStateService } from '../../services/places-state.service';
import { Place } from '../../model/place';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(
    private placesStateService: PlacesStateService,
    private route: ActivatedRoute
  ) { }

  title: string;
  topPlaces: Place[];

  ngOnInit() {

    this.placesStateService.getPlaces().subscribe(  
      places => {
        console.log("success!");
        this.topPlaces = places.sort((a,b) => +a.visits < +b.visits ? 1 : -1);
      }
    );

    this.title = this.route.snapshot.data.title;
  }

}
