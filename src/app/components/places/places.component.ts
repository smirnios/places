import { Component, OnInit } from '@angular/core';
import { Place } from '../../model/place';
import { PlacesStateService } from '../../services/places-state.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {

  selectedPlace: Place = new Place();

  constructor(
    private route: ActivatedRoute,
    private placeStateService: PlacesStateService) { }

  ngOnInit() {
    console.log("ngOnInit test");

    this.route.data
      .subscribe((data: { places: Place[] }) => {
        this.placeStateService.places = data.places;
    });
  }

  ngOnChanges() {

  }

}
