import { Component, OnInit, Input } from '@angular/core';
import { Place } from '../../model/place';
import { PlacesStateService } from '../../services/places-state.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.component.html',
  styleUrls: ['./place-detail.component.css']
})
export class PlaceDetailComponent implements OnInit {

  constructor(
    private placeStateService: PlacesStateService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  place: Place;

  ngOnInit() {

    const id = +this.route.snapshot.paramMap.get('id');
    this.placeStateService.getPlace(id)
      .subscribe(place => this.place = place);
  }

  savePlace() {
    this.placeStateService.createOrUpdatePlace(this.place)
      .subscribe(result => {
        console.log(result);
      });
  }

  goBack(){
    this.location.back();
  }

}
