import { TestBed } from '@angular/core/testing';

import { PlacesStateService } from './places-state.service';

describe('PlacesStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlacesStateService = TestBed.get(PlacesStateService);
    expect(service).toBeTruthy();
  });
});
