import { Injectable } from '@angular/core';
import { Place } from '../model/place';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  url = environment.url;

  constructor(private httpClient: HttpClient) { }

  getPlaces(): Observable<Place[]>{
    return this.httpClient.get<Place[]>(this.url);
  }

  getPlace(id: number): Observable<Place>{
    return this.httpClient.get<Place>(this.url + "/" + id);
  }

  updatePlace(place: Place): Observable<Place>{
    return this.httpClient.put<Place>(this.url + "/" + place.id, place);
  }

  createPlace(place: Place): Observable<Place>{
    return this.httpClient.post<Place>(this.url, place);
  }
}
