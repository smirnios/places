import { Injectable } from '@angular/core';
import { Place } from '../model/place';
import { PlaceService } from './place.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlacesStateService {

  places: Place[] = [];

  constructor(private placeService: PlaceService) { }

  getPlaces(): Observable<Place[]>{
    return this.placeService.getPlaces().pipe(
      map(places => {
        this.places = places;
        return places;
      })
    );
  }

  getPlace(id: number): Observable<Place>{
    return this.placeService.getPlace(id);
  }

  updatePlace(place: Place): Observable<Place>{
    return this.placeService.updatePlace(place);
  }

  createPlace(place: Place): Observable<Place>{
    return this.placeService.createPlace(place).pipe(
      map(place => {
        this.places.push(place);
        return place;
      })
    );
  }

  createOrUpdatePlace(place: Place): Observable<Place>{
    if(place.id){
      return this.updatePlace(place);
    } else {
      return this.createPlace(place);
    }
  }
}
