import { Injectable }             from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
}                                 from '@angular/router';
import { Observable}  from 'rxjs';
import { PlacesStateService } from '../services/places-state.service';
import { Place } from '../model/place';
 
@Injectable({
  providedIn: 'root',
})
export class PlacesResolverService implements Resolve<Place[]> {
  constructor(
    private placesStateService: PlacesStateService, 
    private router: Router) {}
 
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<Place[]> | Observable<never> {
    let id = route.paramMap.get('id');
 
    return this.placesStateService.getPlaces();
    // extra logic in the resolver?
    // .pipe(
    //   take(1),
    //   mergeMap(crisis => {
    //     if (crisis) {
    //       return of(crisis);
    //     } else { // id not found
    //       this.router.navigate(['/welcome']);
    //       return EMPTY;
    //     }
    //   })
    // );
  }
}