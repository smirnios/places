import { TestBed } from '@angular/core/testing';

import { PlacesResolverService } from './places-resolver.service';

describe('PlacesResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlacesResolverService = TestBed.get(PlacesResolverService);
    expect(service).toBeTruthy();
  });
});
